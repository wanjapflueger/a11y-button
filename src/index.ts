import { v4 as uuid } from 'uuid';

interface ButtonInterface {
  /** Any button or anchorlink element */
  el: HTMLButtonElement | HTMLAnchorElement;

  /** Text for title and aria-label. Screen readers will read this text only. */
  title: string;

  /** If true add attribute [aria-busy="true"] to element on click. The [aria-busy="true"] will always be added to element inside HTMLFormElement on form submit. */
  busyOnClick?: boolean;

  /**
   * ISO 639-1 Language Code. By default the button will have the same language as the document
   * @see {@link https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes}
   */
  lang?: string | undefined;
}

/**
 * A fully accessible button
 * @example
 *   Button({
 *     el: document.createElement('button'),
 *     title: 'Button label for screen reader',
 *     busyOnClick: true,
 *     lang: 'en',
 *   });
 */
export const Button = (item: ButtonInterface) => {
  // Language
  item.el.lang = item.lang || document.documentElement.lang;

  // Bind Event Listeners
  item.el.addEventListener('click', () => {
    const setBusy = () => item.el.setAttribute('aria-busy', 'true');
    if (item.busyOnClick) setBusy();

    // If button submits a form, bind an Event Listener to that form
    const form = item.el.closest('form');

    // and set busy state on submit button on form submit
    if (form && form.contains(item.el))
      form.addEventListener('submit', setBusy);
  });

  // Titles and labels
  const label = document.createElement('span');
  label.id = uuid();
  label.innerText = item.title;
  label.classList.add('visuallyhidden');
  item.el.title = item.title;
  item.el.setAttribute('aria-labelledby', label.id);
  item.el.append(label);

  return item.el;
};
