# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.0.7] - 2021-06-22

### Changed

- Moved `uuid` to dependencies

## [v1.0.6] - 2021-06-19

### Changed

- parameter `lang` can now be `undefined` (was only optional before).

## [v1.0.5] - 2021-06-15

### Changed

- Removed redundant attribute `aria-label`

## [v1.0.4] - 2021-05-16

### Changed

- replaced badge source fury.io with shields.io

## [v1.0.3] - 2021-05-16

### Added

- badge for npm version
- badge for bundle size

## [v1.0.2] - 2021-05-16

- Link to video is now only a link since npm does not allow for embedded video contents

## [v1.0.1] - 2021-05-16

### Changed

- Link to video is now absolute so we can dispaly it on npmjs.com
- Documentation chapter „Usage”

## [v1.0.0] - 2021-05-16

### Added

- gitlab-ci
- Readme
- Files for accessible button

## [v0.0.1] - 2021-05-14

### Added

- Initial release

[unreleased]: https://gitlab.com/wanjapflueger/a11y-button
[v1.0.7]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.1...v1.0.2
[v1.0.1]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.com/wanjapflueger/a11y-button/-/compare/v0.0.1...v1.0.0
[v0.0.1]: https://gitlab.com/wanjapflueger/a11y-button/-/tree/v0.0.1
