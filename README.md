# A11Y Button

Improve the accessibility of an existing button or anchor link element.

[![Pipeline status](https://gitlab.com/wanjapflueger/a11y-button/badges/master/pipeline.svg)](https://gitlab.com/wanjapflueger/a11y-button/-/pipelines)
![npm](https://img.shields.io/npm/v/@wanjapflueger/a11y-button)
![npm bundle size](https://img.shields.io/bundlephobia/min/@wanjapflueger/a11y-button)

---

## Table of contents

<!-- TOC -->

- [A11Y Button](#a11y-button)
  - [Table of contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
    - [Options](#options)
  - [Motivation](#motivation)
    - [Busy state](#busy-state)
      - [Solution approach for busy state](#solution-approach-for-busy-state)
    - [Multilanguage environments](#multilanguage-environments)
      - [Solution approach for multilingual environments](#solution-approach-for-multilingual-environments)
      - [Visually hidden elements](#visually-hidden-elements)
      - [Notes](#notes)
  - [Accessibility Compliance Report](#accessibility-compliance-report)

<!-- /TOC -->

---

## Install

```bash
npm i @wanjapflueger/a11y-button
```

## Usage

1. Require/Import module

    ```js
    import { Button } from "@wanjapflueger/a11y-button";

    // or

    var Button = require("@wanjapflueger/a11y-button");
    ```

1. Create a button

    ```js
    /** @type {HTMLButtonElement | HTMLAnchorElement} Any button or anchorlink element */
    let button;

    // Create a new button
    button = document.createElement('button');

    // or use an existing button
    button = getElementById('foo');
    ```

1. Add stuff to button

    ```js
    // add text
    button.innerHTML = `I will not be read by a screen reader`;

    // or elements
    button.append = document.createElement('span');
    ```

1. Make the button accessible

    ```js
    const config = {
      el: button,
      title: 'Button label for screen reader',
      busyOnClick: true,
      lang: 'en',
    };

    // with require
    Button.Button(config);

    // or with import
    Button(config);
    ```

**In short:**

```js
import { Button } from "@wanjapflueger/a11y-button";
const button = document.createElement('button');
button.innerHTML = `I will not be read by a screen reader`;
Button({
  el: button,
  title: 'Button label for screen reader',
  busyOnClick: true,
  lang: 'en',
});
```

### Options

See `ButtonInterface` in [src/index.ts](src/index.ts).

| Property      | Required | Description                                                                                                                                                | Example                                                           |
| ------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
| `el`          | ✅ Yes      | A button or an anchor link element.                                                                                                                        | `document.createElement('button')`, `document.createElement('a')`, `document.getElementById('my-button-id')` |
| `title`       | ✅ Yes      | Text for screen reader                                                                                                                                      | `Lorem Ipsum`                                                     |
| `busyOnClick` | ❌ No      | If `true` add attribute `[aria-busy="true"]` to element on _click_. The `[aria-busy="true"]` will always be added to element inside `HTMLFormElement` on form _submit_ (see [Busy state](#busy-state). | `true` or `false`                                                 |
| `lang`        | ❌ No      | ISO 639-1 Language Code. By default the button will have the same language as the document. Helps screen readers in multi-lang environments (see [Multilanguage environments](#multilanguage-environments))                                                                                    | `en`, `de`                                                        |

## Motivation

### Busy state

Imagine buying a product at an online shop. You add your shipping address and payment method and then hit the „Place Order” button.

```html
<button>Place Order</button>
```

The speed at which the page responds can be affected by many factors.

For example, if the user has a poor internet connection, it may take a while before visual feedback is displayed. He may wonder why nothing is happening, even though something is happening in the background.

#### Solution approach for busy state

By adding an attribute `[aria-busy]`, once the user has clicked on the button, we can add visual feedback at that very moment.

For example we can add a CSS animation like [this](https://gitlab.com/wanjapflueger/a11y-button/-/raw/master/docs/shine.mp4):

```scss
button[aria-busy='true'] {
  animation: 1.5s shine linear infinite;
  background: linear-gradient(110deg, rgba(#fff, 0) 50%, rgba(#fff, 0.2) 70%, rgba(#fff, 0) 90%);
  background-size: 200% 100%;
}

@keyframes shine {
  to {
    background-position-x: -200%;
  }
}
```

### Multilanguage environments

Screen readers misinterprete the language of button elements in some cases.

In this example we create an _English_ document with the language attribute `en`. The system language of the users device is set to _German_.

```html
<html lang="en">
...
<button id="buttonA" aria-label="I am a text">Button A</button>

<button id="buttonB" aria-label="I am a text" lang="en">Button B</button>

<button id="buttonC" aria-label="Ich bin ein Text" lang="de">Button C</button>
...
</html>
```

Screen readers prefer the system language over the document language for button elements. The given example code produces these results:

| Button id | Expected language       | Associated `lang` attribute | Language read by screen reader | Correct language |
| --------- | ----------------------- | --------------------------- | ------------------------------ | ---------------- |
| buttonA   | 🇺🇸 English | `<html>`                    | 🇩🇪 German                    | ❌ No            |
| buttonB   | 🇺🇸 English | `<button>`                  | 🇩🇪 German                    | ❌ No            |
| buttonC   | 🇩🇪 German             | `<button>`                  | 🇩🇪 German                    | ✅ Yes           |

#### Solution approach for multilingual environments

Given the example of an English document with the language attribute `en`, we can see that screen readers read the correct language of button elements if the language attribute `lang` is being used in combination with `aria-labelledby`.

```html
<html lang="en">
...
<button id="buttonA" aria-labelledby="labelForButtonA" lang="en">
  Button A
  <span id="labelForButtonA" class="visuallyhidden">I am a text</span>
</button>

<button id="buttonB" aria-labelledby="labelForButtonB" lang="de">
  Button B
  <span id="labelForButtonB" class="visuallyhidden">Ich bin ein Text</span>
</button>

<button id="buttonB" aria-labelledby="labelForButtonC" lang="fr">
  Button C
  <span id="labelForButtonC" class="visuallyhidden">Je suis un texte</span>
</button>
...
</html>
```

The screen reader ignores system and document language. The given example code produces these results:

| Button id | Expected language       | Associated `lang` attribute | Language read by screen reader | Correct language |
| --------- | ----------------------- | --------------------------- | ------------------------------ | ---------------- |
| buttonA   | 🇺🇸 English | `<button>`                  | 🇺🇸 English        | ✅ Yes           |
| buttonB   | 🇩🇪 German             | `<button>`                  | 🇩🇪 German                    | ✅ Yes           |
| buttonC   | 🇫🇷 French             | `<button>`                  | 🇫🇷 French                    | ✅ Yes           |

#### Visually hidden elements

You may add this code to your project to hide elements visually. Visually hidden elements are still accessible to screen readers.

```scss
/// Hide visually (still accessible by screen readers)
/// @link https://a11yproject.com/posts/how-to-hide-content/

@mixin hide-visually() {
  position: absolute;
  height: 1px;
  width: 1px;
  overflow: hidden;
  clip: rect(1px, 1px, 1px, 1px);
  white-space: nowrap;
}

.visuallyhidden {
  @include hide-visually();
}
```

#### Notes

Keep in mind that using `aria-label` or `aria-labelledby` means, that screen readers cannot read any text on a button, that is not a label in this context.

```html
<button aria-labelledby="buttonLabel">
  Apple
  <span id="buttonLabel">Banana</span>
</button>
```

As shown in the code example above, screen readers will always read `Banana` but never `Apple`.

## Accessibility Compliance Report

[WCAG](https://www.w3.org/WAI/WCAG21/quickref/) Level: **AAA** [^1]


| Browser              | Platform      | Screen reader                                                               | Passed |
| -------------------- | ------------- | -------------------------------------------------------------------------- | ------ |
| Chrome 90.0.4430.212 | MacOS 10.15.7 | [VoiceOver](https://www.apple.com/de/accessibility/vision/)                | ✅     |
| Chrome 90.0.4430.210 | Android 10    | [Talkback](https://support.google.com/accessibility/android#topic=6007234) | ✅     |

---

[^1]: This information refers only to the technical aspects of the component, not to the design or the editorial handling of any content.
