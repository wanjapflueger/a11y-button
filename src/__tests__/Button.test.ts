import { Button } from '../index';

const button = Button({
  el: document.createElement('button'),
  title: 'Button',
  busyOnClick: true,
  lang: 'en',
});

const anchorlink = Button({
  el: document.createElement('a'),
  title: 'Anchorlink',
});

test('Element can be HTMLButtonElement', () => {
  expect(button.nodeName).toBe('BUTTON');
});

test('Element can be HTMLAnchorElement', () => {
  expect(anchorlink.nodeName).toBe('A');
});

test('Element has titles and labels', () => {
  expect(button.title).toBe('Button');
  expect(button.getAttribute('aria-labelledby')).not.toBe('null');
  const buttonCaption: HTMLSpanElement | null = button.querySelector(
    `[id="${button.getAttribute('aria-labelledby')}"]`,
  );
  if (button.getAttribute('aria-labelledby') && buttonCaption)
    expect(buttonCaption.innerText).toBe('Button');

  expect(anchorlink.title).toBe('Anchorlink');
  expect(anchorlink.getAttribute('aria-labelledby')).not.toBe('null');
  const anchorLinkCaption: HTMLSpanElement | null = anchorlink.querySelector(
    `[id="${button.getAttribute('aria-labelledby')}"]`,
  );
  if (anchorlink.getAttribute('aria-labelledby') && anchorLinkCaption)
    expect(anchorLinkCaption.innerText).toBe('Anchorlink');
});

test('Element has language defined in options', () => {
  expect(button.lang).toBe('en');
});

test('Element has no language defined in options', () => {
  expect(anchorlink.lang).toBe(document.documentElement.lang);
});
